#########################
### build environment ###
#########################

# base image
FROM node:carbon as builder

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY BonusMode/package.json /app/
RUN npm install

# add app
COPY ./BonusMode /app/

# generate build
RUN npm run release

##################
### production ###
##################

# base image
FROM nginx:latest

# copy artifact build from the 'build environment'
COPY --from=builder /app/dist_prod /usr/share/nginx/html

COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

# run nginx
CMD ["nginx", "-g", "daemon off;"]
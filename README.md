# BonusMode Angular Web

### Build:
`docker build -t bonusmode_web .`

### Run:
`docker run -d -p 8080:80 --name bonusmode_web bonusmode_web`

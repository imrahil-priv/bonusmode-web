import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { AuthGuard } from '../core/services/auth.guard';
import { MaterialModule } from '../material.module';
import { ContractDetailsComponent } from './components/contract-details/contract-details.component';
import { ContractListComponent } from './components/contract-list/contract-list.component';
import { ContractNewComponent } from './components/contract-new/contract-new.component';
import { ContractsService } from './services/contracts.service';

const routes: Routes = [
  { path: 'contracts', component: ContractListComponent, canActivate: [AuthGuard] },
  { path: 'contracts/new', component: ContractNewComponent, canActivate: [AuthGuard] },
  { path: 'contracts/:id/details', component: ContractDetailsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    ContractListComponent,
    ContractNewComponent,
    ContractDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MaterialModule,
    CoreModule
  ],
  providers: [
    ContractsService
  ],
  exports: [RouterModule]
})
export class ContractsModule { }

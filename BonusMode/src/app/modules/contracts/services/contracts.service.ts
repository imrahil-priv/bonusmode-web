import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Contract, ContractList, ContractType, ContractTypeList } from '../../../shared/models/contract.model';

@Injectable()
export class ContractsService {

  constructor(private http: HttpClient) { }

  public getContracts(): Observable<Contract[]> {
    const url = `${environment.apiUrl}/contracts`;

    return this.http.get<ContractList>(url)
      .pipe(
        map(contractList => contractList.contracts)
      );
  }

  public getContractDetails(contractId: number): Observable<Contract> {
    const url = `${environment.apiUrl}/contracts/${contractId}`;

    return this.http.get<Contract>(url)
      .pipe(
        map(contractDetails => new Contract(contractDetails))
      );
  }

  public getContractTypes(): Observable<ContractType[]> {
    const url = `${environment.apiUrl}/contracts/types`;

    return this.http.get<ContractTypeList>(url)
      .pipe(
        map(contractTypeList => contractTypeList.contractTypes)
      );
  }

  public createContract(contract: Contract): Observable<Contract> {
    const url = `${environment.apiUrl}/contracts`;

    return this.http.post<Contract>(url, contract);
  }

  public updateContract(contractId: number, contract: Contract): Observable<Contract> {
    const url = `${environment.apiUrl}/contracts/${contractId}`;

    return this.http.put<Contract>(url, contract);
  }
}

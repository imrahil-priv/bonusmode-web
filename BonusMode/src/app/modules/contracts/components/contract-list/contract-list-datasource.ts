import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { Contract } from '../../../../shared/models/contract.model';
import { ContractsService } from '../../services/contracts.service';

export class ContractListDataSource extends DataSource<Contract> {
  public loading$: Observable<boolean>;
  private filter$: BehaviorSubject<{searchTerm: string}> = new BehaviorSubject({searchTerm: null});
  private loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private contractsService: ContractsService) {
    super();

    this.loading$ = this.loadingSubject.asObservable();
  }

  public connect(): Observable<Contract[]> {
    const contracts$: Observable<Contract[]> = this.contractsService.getContracts()
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      );

    return combineLatest(contracts$, this.filter$)
    .pipe(
      map(latestValues => {
        const [contracts, filterData] = latestValues;
        if (!filterData.searchTerm) { return contracts; }

        return contracts.filter(contract => {
          return contract.number.toLocaleLowerCase().includes(filterData.searchTerm.toLocaleLowerCase());
        });
      })
    );
  }

  public disconnect(): void {
    // stub
  }

  public filter(searchTerm: string): void {
    this.filter$.next({searchTerm});
  }
}

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContractsService } from '../../services/contracts.service';
import { Contract } from 'src/app/shared/models/contract.model';
import { ContractListDataSource } from './contract-list-datasource';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.scss']
})
export class ContractListComponent implements OnInit {
  public dataSource: ContractListDataSource;
  public displayedColumns = ['id', 'number', 'description', 'amount', 'details'];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private contractsService: ContractsService,
              private breakpointObserver: BreakpointObserver,
              public dialog: MatDialog) {}

  public ngOnInit(): void {
    this.dataSource = new ContractListDataSource(this.contractsService);
  }

  public getContractDetails(contractId: number): void {
    // stub
  }

  public applyFilter(filterValue: string): void {
    this.dataSource.filter(filterValue.trim().toLowerCase());
  }

  public finishContract(contract: Contract): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        id: 1,
        title: 'Warning!',
        question: 'Are you sure you want to finish this contract?'
    };

    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        contract.finished = true;
        this.contractsService.updateContract(contract.contractId, contract).subscribe();
      }
    });
  }
}

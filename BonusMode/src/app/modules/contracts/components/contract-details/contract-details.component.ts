import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Contract } from '../../../../shared/models/contract.model';
import { ContractsService } from '../../services/contracts.service';

@Component({
  selector: 'app-contract-details',
  templateUrl: './contract-details.component.html',
  styleUrls: ['./contract-details.component.scss']
})
export class ContractDetailsComponent implements OnInit, OnDestroy {
  public contract: Contract;

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  protected onDestroy = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private breakpointObserver: BreakpointObserver,
              private contractsService: ContractsService) { }

  public ngOnInit(): void {
    this.route.params
      .pipe(takeUntil(this.onDestroy))
      .subscribe((params) => {
        const id = +params['id'];

        this.contractsService.getContractDetails(id)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((result: Contract) => {
            this.contract = result;
          });
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}

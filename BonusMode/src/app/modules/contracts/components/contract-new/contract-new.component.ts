import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { EmployeesService } from '../../../../modules/employees/services/employees.service';
import { Contract, ContractType } from '../../../../shared/models/contract.model';
import { Employee } from '../../../../shared/models/employee.model';
import { ContractsService } from '../../services/contracts.service';

@Component({
  selector: 'app-contract-new',
  templateUrl: './contract-new.component.html',
  styleUrls: ['./contract-new.component.scss']
})
export class ContractNewComponent implements OnInit, OnDestroy {
  public contractForm: FormGroup;
  public employees: Employee[];
  public contractTypes: ContractType[];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );

  protected onDestroy = new Subject<void>();

  constructor(private contractsService: ContractsService,
              private employeesService: EmployeesService,
              private breakpointObserver: BreakpointObserver,
              private router: Router,
              private fb: FormBuilder) { }

  public ngOnInit(): void {
    const employeesList$: Observable<Employee[]> = this.employeesService.employeesList;
    const contractTypes$: Observable<ContractType[]> = this.contractsService.getContractTypes();
    this.employeesService.getEmployees();

    combineLatest([employeesList$, contractTypes$])
      .pipe(takeUntil(this.onDestroy))
      .subscribe((results: any) => {
        [this.employees, this.contractTypes] = results;

        this.contractForm = this.fb.group({
          number: ['', Validators.required],
          description: ['', Validators.required],
          amount: ['', Validators.required],
          contractType: ['', Validators.required],
          contractEmployees: this.fb.array([ this.createContractEmployee() ])
        });
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  public compareByContractTypeId(r1: ContractType, r2: ContractType): boolean {
    return r1 && r2 && r1.contractTypeId === r2.contractTypeId;
  }

  public compareByEmployeeId(r1: Employee, r2: Employee): boolean {
    return r1 && r2 && r1.employeeId === r2.employeeId;
  }

  public addContractEmployee(): void {
    const contractEmployees: FormArray = this.contractForm.get('contractEmployees') as FormArray;
    contractEmployees.push(this.createContractEmployee());
  }

  public deleteContractEmployee(index: number): void {
    const contractEmployees: FormArray = this.contractForm.get('contractEmployees') as FormArray;
    contractEmployees.removeAt(index);
  }

  public save(): void {
    if (this.contractForm.valid) {
      const contractData: Contract = this.contractForm.value;

      this.contractsService.createContract(contractData)
        .pipe(takeUntil(this.onDestroy))
        .subscribe(() => {
          this.router.navigate(['/contracts'])
            .catch((error) => console.error(error));
        });
    }
  }

  private createContractEmployee(): FormGroup {
    return this.fb.group({
      scopeValuation: ['', Validators.required],
      scopeMeasurement: ['', Validators.required],
      employee: ['', Validators.required]
    });
  }
}

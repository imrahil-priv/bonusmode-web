import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private fb: FormBuilder,
              private authService: AuthService) { }

  public ngOnInit(): void {
    this.authService.logout();

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  public login(): void {
    this.authService.login();
    this.authService.redirectAfterLogin();
  }

  public getEmailErrorMessage(): string {
    return this.loginForm.get('email').hasError('required') ? 'Proszę podać email' :
        this.loginForm.get('email').hasError('email') ? 'Adres email nie jest poprawny' :
            '';
  }
}

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { SidenavService } from '../../services/sidenav.service';

export interface NavLink {
  name: string;
  target: string;
  count?: number;
  disabled?: boolean;
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  @ViewChild('sidenav') set content(sidenav: MatSidenav) {
    this.sidenavService.setSidenav(sidenav);
  }

  public links: NavLink[] = [
    {name: 'Dashboard', target: '/dashboard', count: 4},
    {name: 'Lista umów', target: '/contracts'},
    {name: 'Lista pracowników', target: '/employees'},
    {name: 'Raporty', target: '/reports', disabled: true},
    {name: 'Ustawienia', target: '/settings'},
    {name: 'Wyjście', target: '/login'}
  ];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private sidenavService: SidenavService,
              public authService: AuthService) {}

  public ngOnInit(): void {
    // ...
  }

  public closeSideNavOnClick(): void {
    if (this.breakpointObserver.isMatched(Breakpoints.Handset)) {
      this.sidenavService.close()
        .catch((error) => console.error(error));
    }
  }
}

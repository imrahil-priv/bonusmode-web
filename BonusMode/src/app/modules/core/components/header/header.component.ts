import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SidenavService } from '../../services/sidenav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  /* tslint:disable:no-input-rename */
  @Input('headerTitle') public headerTitle: string;
  @Input('showBackButton') public showBackButton: boolean;
  @Input('backLink') public backLink: string = '/';
  /* tslint:enable:no-input-rename */

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private sidenavService: SidenavService,
              private breakpointObserver: BreakpointObserver) { }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

const DEFAULT_AFTERLOGIN_REDIRECT = 'dashboard';

@Injectable()
export class AuthService {
  public isLoggedIn$: Observable<boolean>;
  private authSubject = new BehaviorSubject<boolean>(true);
  private _redirectUrl: string;

  constructor(private router: Router) {
    this.isLoggedIn$ = this.authSubject.asObservable();
  }

  public login(): void {
    this.authSubject.next(true);
  }

  public logout(): void {
    setTimeout(() => this.authSubject.next(false));
    this.router.navigate(['/login'])
      .catch((error) => console.error(error));
  }

  public redirectToLogin(url: string): Promise<boolean> {
    this._redirectUrl = url ? url : null;

    return this.router.navigate(['/login']);
  }

  public redirectAfterLogin(): void {
    this.router.navigate([this.redirectUrl])
      .catch((err) => console.error('Redirect after login failed: \n', err));
  }

  public get redirectUrl(): string {
    return this._redirectUrl ? this._redirectUrl : DEFAULT_AFTERLOGIN_REDIRECT;
  }
}

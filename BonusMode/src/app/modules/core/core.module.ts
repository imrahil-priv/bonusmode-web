import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { AuthGuard } from './services/auth.guard';
import { AuthService } from './services/auth.service';
import { SidenavService } from './services/sidenav.service';

@NgModule({
  declarations: [
    DashboardComponent,
    NavigationComponent,
    HeaderComponent,
    LoginComponent,
    TruncatePipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [
    SidenavService,
    AuthService,
    AuthGuard
  ],
  exports: [
    LoginComponent,
    DashboardComponent,
    NavigationComponent,
    HeaderComponent,
    TruncatePipe
  ]
})
export class CoreModule { }

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Settings } from '../../../shared/models/settings.model';

@Injectable()
export class SettingsService {

  constructor(private http: HttpClient) { }

  public getSettings(): Observable<Settings> {
    const url = `${environment.apiUrl}/settings`;

    return this.http.get<Settings>(url);
  }

  public updateSettings(settings: Settings): Observable<Settings> {
    const url = `${environment.apiUrl}/settings`;

    return this.http.put<Settings>(url, settings);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { AuthGuard } from '../core/services/auth.guard';
import { MaterialModule } from '../material.module';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsService } from './services/settings.service';

const routes: Routes = [
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MaterialModule,
    CoreModule
  ],
  providers: [
    SettingsService
  ],
  exports: [RouterModule]
})
export class SettingsModule { }

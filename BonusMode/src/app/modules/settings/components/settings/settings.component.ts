import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Settings } from '../../../../shared/models/settings.model';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  public settingsForm: FormGroup;

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  protected onDestroy = new Subject<void>();

  constructor(private settingsService: SettingsService,
              private breakpointObserver: BreakpointObserver,
              private fb: FormBuilder) { }

  public ngOnInit(): void {
    this.settingsService.getSettings()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((settings: Settings) => {
        console.log(settings);

        this.settingsForm = this.fb.group({
          settingId: [null],
          valuationBonus: [null, Validators.required],
          measurementBonus: [null, Validators.required],
          finalizationBonus: [null, Validators.required]
        });

        this.settingsForm.patchValue(settings);
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  public saveSettings(): void {
    if (this.settingsForm.valid) {
      const settings: Settings = this.settingsForm.value;

      this.settingsService.updateSettings(settings)
        .pipe(takeUntil(this.onDestroy))
        .subscribe((updatedSettings: Settings) => {
          this.settingsForm.patchValue(updatedSettings);
        });
    }
  }
}

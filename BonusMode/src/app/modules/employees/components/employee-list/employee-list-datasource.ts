import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Employee } from '../../../../shared/models/employee.model';
import { EmployeesService } from '../../services/employees.service';

export class EmployeeListDataSource extends DataSource<Employee> {
  public loading$: Observable<boolean>;
  private filter$: BehaviorSubject<{searchTerm: string}> = new BehaviorSubject({searchTerm: null});
  private loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(private employeesService: EmployeesService) {
    super();

    this.loading$ = this.loadingSubject.asObservable();
  }

  public connect(): Observable<Employee[]> {
    const employees$: Observable<Employee[]> = this.employeesService.employeesList
      .pipe(
        catchError(() => of([])),
        tap(() => this.loadingSubject.next(false))
      );

    return combineLatest(employees$, this.filter$)
      .pipe(
        map(latestValues => {
          const [employees, filterData] = latestValues;
          if (!filterData.searchTerm) { return employees; }

          return employees.filter(employee => {
            return employee.fullName.toLocaleLowerCase().includes(filterData.searchTerm.toLocaleLowerCase()) ||
                      employee.role.roleName.toLocaleLowerCase().includes(filterData.searchTerm.toLocaleLowerCase());
          });
        })
      );
  }

  public disconnect(): void {
    this.filter$.complete();
  }

  public filter(searchTerm: string): void {
    this.filter$.next({searchTerm});
  }
}

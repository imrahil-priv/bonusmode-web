import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EmployeesService } from '../../services/employees.service';
import { EmployeeListDataSource } from './employee-list-datasource';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit {
  public dataSource: EmployeeListDataSource;
  public displayedColumns = ['id', 'name', 'roleName', 'details'];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private employeesService: EmployeesService,
              private breakpointObserver: BreakpointObserver) {}

  public ngOnInit(): void {
    this.dataSource = new EmployeeListDataSource(this.employeesService);
    this.employeesService.getEmployees();
  }

  public applyFilter(filterValue: string): void {
    this.dataSource.filter(filterValue.trim().toLowerCase());
  }

  public deleteEmployee(employeeId: number): void {
    this.employeesService.deleteEmployee(employeeId)
      .subscribe(() => {
        this.employeesService.getEmployees();
      });
  }
}

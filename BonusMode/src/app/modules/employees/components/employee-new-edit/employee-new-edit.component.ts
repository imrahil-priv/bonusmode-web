import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Employee } from '../../../../shared/models/employee.model';
import { Role } from '../../../../shared/models/role.model';
import { EmployeesService } from '../../services/employees.service';
import { RolesService } from '../../services/roles.service';

@Component({
  selector: 'app-employee-new-edit',
  templateUrl: './employee-new-edit.component.html',
  styleUrls: ['./employee-new-edit.component.scss']
})
export class EmployeeNewEditComponent implements OnInit, OnDestroy {
  public employeeForm: FormGroup;
  public roles: Role[];

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  protected onDestroy = new Subject<void>();

  private employeeId: number;
  private isEditMode: boolean = true;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employeesService: EmployeesService,
              private rolesService: RolesService,
              private breakpointObserver: BreakpointObserver,
              private fb: FormBuilder) { }

  public ngOnInit(): void {
    this.route.params
      .pipe(takeUntil(this.onDestroy))
      .subscribe((params) => {
        this.employeeId = +params['id'];

        const roles$: Observable<Role[]> = this.rolesService.getRoles();

        if (this.employeeId) {
          const employeeDetails$: Observable<Employee> = this.employeesService.getEmployeeDetails(this.employeeId);

          forkJoin([employeeDetails$, roles$])
            .pipe(takeUntil(this.onDestroy))
            .subscribe((results) => {
              let employee: Employee;

              [employee, this.roles] = results;

              this.employeeForm = this.fb.group({
                firstName: [employee.firstName, Validators.required],
                lastName: [employee.lastName, Validators.required],
                role: [employee.role, Validators.required]
              });
            });
        } else {
          this.isEditMode = false;

          roles$
            .pipe(takeUntil(this.onDestroy))
            .subscribe((results: Role[]) => {
              this.roles = results;

              this.employeeForm = this.fb.group({
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                role: ['', Validators.required]
              });
            });
        }
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  public compareByRole(r1: Role, r2: Role): boolean {
    return r1 && r2 && r1.roleId === r2.roleId;
  }

  public save(): void {
    if (this.employeeForm.valid) {
      const employeeData: Employee = this.employeeForm.value;
      let call$: Observable<Employee>;

      if (this.isEditMode) {
        employeeData.employeeId = this.employeeId;

        call$ = this.employeesService.updateEmployee(this.employeeId, employeeData);
      } else {
        call$ = this.employeesService.createEmployee(employeeData);
      }

      call$
        .pipe(takeUntil(this.onDestroy))
        .subscribe(() => {
          this.router.navigate(['/employees'])
            .catch((error) => console.error(error));
        });
    }
  }
}

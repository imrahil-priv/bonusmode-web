import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { AuthGuard } from '../core/services/auth.guard';
import { MaterialModule } from '../material.module';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeNewEditComponent } from './components/employee-new-edit/employee-new-edit.component';
import { EmployeesService } from './services/employees.service';
import { RolesService } from './services/roles.service';

const routes: Routes = [
  { path: 'employees', component: EmployeeListComponent, canActivate: [AuthGuard] },
  { path: 'employees/new', component: EmployeeNewEditComponent, canActivate: [AuthGuard] },
  { path: 'employees/:id/edit', component: EmployeeNewEditComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeNewEditComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MaterialModule,
    CoreModule
  ],
  providers: [
    EmployeesService,
    RolesService
  ],
  exports: [RouterModule]
})
export class EmployeesModule { }

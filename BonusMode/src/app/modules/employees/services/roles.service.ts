import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Role, RoleList } from '../../../shared/models/role.model';

@Injectable()
export class RolesService {
  private data: Role[] = [];

  constructor(private http: HttpClient) { }

  public getRoles(): Observable<Role[]> {
    if (this.data.length > 0) {
      return of(this.data);
    }

    const url = `${environment.apiUrl}/roles`;

    return this.http.get<RoleList>(url)
      .pipe(
        map((rolesList: RoleList) => this.data = rolesList.roles)
      );
  }
}

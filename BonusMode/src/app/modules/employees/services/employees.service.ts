import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Employee, EmployeeList } from '../../../shared/models/employee.model';

@Injectable()
export class EmployeesService {
  public employeesList: Observable<Employee[]>;
  private _employees: Subject<Employee[]> = new Subject<Employee[]>();

  constructor(private http: HttpClient) {
    this.employeesList = this._employees.asObservable();
  }

  public getEmployees(): void {
    const url = `${environment.apiUrl}/employees`;

    this.http.get<EmployeeList>(url)
      .pipe(
        map((data: EmployeeList) => data.employees
          .map(employee => {
            return new Employee(employee);
          })
        )
      )
      .subscribe((employees: Employee[]) => {
        this._employees.next(employees);
      });
  }

  public getEmployeeDetails(employeeId: number): Observable<Employee> {
    const url = `${environment.apiUrl}/employees/${employeeId}`;

    return this.http.get<Employee>(url)
      .pipe(
        map(employeeProperties => new Employee(employeeProperties))
      );
  }

  public createEmployee(employee: Employee): Observable<Employee> {
    const url = `${environment.apiUrl}/employees`;

    return this.http.post<Employee>(url, employee);
  }

  public updateEmployee(employeeId: number, employee: Employee): Observable<Employee> {
    const url = `${environment.apiUrl}/employees/${employeeId}`;

    return this.http.put<Employee>(url, employee);
  }

  public deleteEmployee(employeeId: number): Observable<Employee> {
    const url = `${environment.apiUrl}/employees/${employeeId}`;
    console.log('delete! ' + url);

    return this.http.delete<Employee>(url);
  }
}

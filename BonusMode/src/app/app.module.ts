import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContractsModule } from './modules/contracts/contracts.module';
import { CoreModule } from './modules/core/core.module';
import { EmployeesModule } from './modules/employees/employees.module';
import { MaterialModule } from './modules/material.module';
import { SettingsModule } from './modules/settings/settings.module';
import { ConfirmDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,

    AppRoutingModule,
    CoreModule,
    EmployeesModule,
    ContractsModule,
    SettingsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmDialogComponent
]
})
export class AppModule { }

import { Employee } from './employee.model';

export class ContractList {
  public contracts: Contract[];
}

export class Contract {
  public contractId: number;
  public number: string;
  public description: string;
  public amount: number;
  public createdAt: string;
  public updatedAt: string;
  public finished: boolean;
  public accounted: boolean;

  public contractType: ContractType;
  public contractEmployees: ContractEmployee[];

  constructor(data: Object | Contract) {
    Object.assign(this, data);

    this.contractEmployees.map((item: ContractEmployee) => {
      item.employee = new Employee(item.employee);

      return item;
    });
  }
}

export class ContractEmployee {
  public contractEmployeeId: number;
  public scopeValuation: number;
  public scopeMeasurement: number;
  public employee: Employee;
}

export class ContractTypeList {
  public contractTypes: ContractType[];
}

export class ContractType {
  public contractTypeId: number;
  public name: string;
}

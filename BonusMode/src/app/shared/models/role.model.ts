export class RoleList {
  public roles: Role[];
}

export class Role {
  public roleId: number;
  public roleName: string;
}

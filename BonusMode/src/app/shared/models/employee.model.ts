import { Role } from './role.model';

export class EmployeeList {
  public employees: Employee[];
}

export class Employee {
  public employeeId: number;
  public firstName: string;
  public lastName: string;

  public role: Role;

  constructor(data: Object | Employee) {
    Object.assign(this, data);
  }

  get fullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }
}
